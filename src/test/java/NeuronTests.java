import NeuralNetworkLib.NeuralNetworkError;
import NeuralNetworkLib.Neuron;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test case for Neuron class")
public class NeuronTests {
    static private Neuron neuron;

    @BeforeAll
    static void setupNeuron(){
        neuron = new Neuron(2, Neuron.ActivationFunction.UNIPOLAR);
    }

    @Test
    void constructionTest(){
        assertAll("Number of fields",
                ()->assertEquals(2, neuron.getInputNumber()),
                ()->assertEquals(3, neuron.getInputsNumber()),
                ()->assertEquals(3, neuron.getWeightsNumber()),
                ()->assertEquals(3, neuron.getWeightsDeltasNumber()));
        for (Double weight: neuron.getWeights()){
            assertEquals(0.0, weight);
        }
        for (Double weightsDelta: neuron.getWeightsDeltas()){
            assertEquals(0.0, weightsDelta);
        }
        for (int i = 0; i < neuron.getInputs().size(); ++i){
            if (i == 0){
                assertEquals(1.0, neuron.getInput(i));
            } else {
                assertEquals(0.0, neuron.getInput(i));
            }
        }
    }

    @Test
    void setInputsTest() throws NeuralNetworkError {
        ArrayList<Double> inputs = new ArrayList<>(Arrays.asList(2.0, 3.0));

        neuron.setInput(inputs);
        assertEquals(3, neuron.getInputs().size(), "Expected size is 3 but is "+neuron.getInputs().size());
        assertAll("inputs",
                ()->assertEquals(neuron.getInput(0), 1.0),
                ()->assertEquals(neuron.getInput(1), 2.0),
                ()->assertEquals(neuron.getInput(2), 3.0));
    }

    @Test
    void weightedSumTest() throws NeuralNetworkError {
        ArrayList<Double> inputs = new ArrayList<>(Arrays.asList(2.5, 3.5));
        ArrayList<Double> weights = new ArrayList<>(Arrays.asList(5.5, 2.5, 3.5));
        neuron.setInput(inputs);
        neuron.setWeights(weights);

        assertEquals(24.0, neuron.weightedSum());

        inputs = new ArrayList<>(Arrays.asList(4.0, 6.0));
        weights = new ArrayList<>(Arrays.asList(3.0, 2.0, 4.0));
        neuron.setInput(inputs);
        neuron.setWeights(weights);

        assertEquals(35.0, neuron.weightedSum());
    }

    @Test
    void outputTest() throws NeuralNetworkError {
        ArrayList<Double> inputs = new ArrayList<>(Arrays.asList(2.5, 3.5));
        ArrayList<Double> weights = new ArrayList<>(Arrays.asList(5.5, 2.5, 3.5));
        neuron.setInput(inputs);
        neuron.setWeights(weights);
        neuron.calculateOutput();

        assertTrue(neuron.output()>0.99&&neuron.output()<1.01);

        inputs = new ArrayList<>(Arrays.asList(4.0, 6.0));
        weights = new ArrayList<>(Arrays.asList(0.1, 0.3, 0.5));
        neuron.setInput(inputs);
        neuron.setWeights(weights);
        neuron.calculateOutput();

        assertTrue(neuron.output()>0.9866&&neuron.output()<0.9867);
    }

    @Test
    void drawingWeightsTest() {
        for (int i = 0; i < 100; ++i){
            neuron.drawWeights();
            for (Double weight: neuron.getWeights()){
                assertTrue(weight>=-0.5&&weight<=0.5);
            }
        }
    }

    @Test
    void derivativeTest() throws NeuralNetworkError {
        ArrayList<Double> inputs = new ArrayList<>(Arrays.asList(4.0, 6.0));
        ArrayList<Double> weights = new ArrayList<>(Arrays.asList(0.1, 0.3, 0.5));
        neuron.setInput(inputs);
        neuron.setWeights(weights);
        neuron.calculateOutput();
        neuron.calculateDerivative();
        assertEquals(neuron.output()-Math.pow(neuron.output(), 2), neuron.derivative());
        assertTrue(neuron.derivative()>0.01320&&neuron.derivative()<0.01321, "Derivative: "+neuron.derivative());
    }

    @Test
    void deltaOutputTest() throws NeuralNetworkError {
        ArrayList<Double> inputs = new ArrayList<>(Arrays.asList(4.0, 6.0));
        ArrayList<Double> weights = new ArrayList<>(Arrays.asList(0.1, 0.3, 0.5));
        neuron.setInput(inputs);
        neuron.setWeights(weights);
        neuron.calculateOutput();
        neuron.calculateDerivative();
        double expected = 1.0;
        neuron.calculateDeltaOutputLevel(expected);
        assertEquals((expected-neuron.output())*neuron.derivative(), neuron.getDelta());
        assertTrue(neuron.getDelta()>0.00017&&neuron.getDelta()<0.00018);
    }

    @Test
    void deltaNonOutputTest() throws NeuralNetworkError {
        Neuron con1 = new Neuron(1, Neuron.ActivationFunction.UNIPOLAR);
        Neuron con2 = new Neuron(1, Neuron.ActivationFunction.UNIPOLAR);
        con1.setDelta(1.0);
        con2.setDelta(2.0);

        neuron.connect(con1, 1);
        neuron.connect(con2, 2);

        ArrayList<Double> weights = new ArrayList<>(Arrays.asList(1.0, 2.0, 3.0));

        con1.setWeights(weights);
        con2.setWeights(weights);
        neuron.setDerivative(2.0);
        neuron.setOutputReady(true);
        neuron.setDerivativeReady(true);
        neuron.calculateDeltaNonOutputLevel();

        assertEquals(16.0, neuron.getDelta());
    }

    @Test
    void updateWeightsTest() throws NeuralNetworkError {
        ArrayList<Double> weights = new ArrayList<>(Arrays.asList(1.0, 2.0, 3.0));
        ArrayList<Double> inputs = new ArrayList<>(Arrays.asList(2.0, 3.0));
        neuron.setInput(inputs);
        neuron.setWeights(weights);
        assertEquals(3, neuron.getInputs().size());
        assertEquals(3, neuron.getWeights().size());
        for (int i = 0; i < neuron.getWeights().size(); ++i) {
            assertEquals(weights.get(i), neuron.getWeights().get(i));
        }
        for (int i = 0; i < neuron.getInputs().size(); ++i) {
            if (i == 0)
                assertEquals(1.0, neuron.getInputs().get(i));
            else
                assertEquals(inputs.get(i-1), neuron.getInputs().get(i));
        }
        neuron.setDelta(2.0);
        neuron.updateWeights(0.2);
        assertAll("Weights after update test",
                ()->assertEquals(1.4, neuron.getWeight(0)),
                ()->assertEquals(2.8, neuron.getWeight(1)),
                ()->assertEquals(4.2, neuron.getWeight(2)));
    }
}