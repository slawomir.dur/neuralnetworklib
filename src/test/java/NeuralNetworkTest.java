import NeuralNetworkLib.NeuralNetwork;
import NeuralNetworkLib.NeuralNetworkError;
import NeuralNetworkLib.Neuron;
import NeuralNetworkLib.TrainingData;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class NeuralNetworkTest {
    @Test
    void architectureTest() {
        NeuralNetwork net = new NeuralNetwork(2, 2, 1, Neuron.ActivationFunction.UNIPOLAR, true);
        assertEquals(2, net.Levels());
        assertEquals(3, net.Neurons());
    }

    @Test
    void learnTwoDimXORTest() throws NeuralNetworkError {
        NeuralNetwork net = new NeuralNetwork(2,2,1, Neuron.ActivationFunction.UNIPOLAR, true);
        TrainingData data = new TrainingData();
        data.addTrainingVector(new Double[]{1.0, 1.0}, new Double[]{0.0});
        data.addTrainingVector(new Double[]{0.0, 1.0}, new Double[]{1.0});
        data.addTrainingVector(new Double[]{1.0, 0.0}, new Double[]{1.0});
        data.addTrainingVector(new Double[]{0.0, 0.0}, new Double[]{0.0});
        net.trainWhileErrorNotLessThan(0.1, data);
        double oneoneanswer = net.getAnswer(new ArrayList<>(Arrays.asList(1.0, 1.0))).get(0);
        double onezeroanswer = net.getAnswer(new ArrayList<>(Arrays.asList(1.0, 0.0))).get(0);
        double zerooneanswer = net.getAnswer(new ArrayList<>(Arrays.asList(0.0, 1.0))).get(0);
        double zerozeroanswer = net.getAnswer(new ArrayList<>(Arrays.asList(0.0, 0.0))).get(0);
        assertAll("Answers",
                ()->assertTrue(oneoneanswer<0.15, "1 1: "+oneoneanswer),
                ()->assertTrue(zerooneanswer>0.85, "0 1: "+zerooneanswer),
                ()->assertTrue(onezeroanswer>0.85, "1 0: "+onezeroanswer),
                ()->assertTrue(zerozeroanswer<0.15, "0 0: "+zerozeroanswer));
    }

    @Test
    void threeLevelXORWithCustomValuesTest() throws NeuralNetworkError {
        NeuralNetwork net = new NeuralNetwork(2,2,1, Neuron.ActivationFunction.UNIPOLAR, false);
        TrainingData data = new TrainingData();
        data.addTrainingVector(new Double[]{1.0, 1.0}, new Double[]{0.0});
        data.addTrainingVector(new Double[]{0.0, 1.0}, new Double[]{1.0});
        data.addTrainingVector(new Double[]{1.0, 0.0}, new Double[]{1.0});
        data.addTrainingVector(new Double[]{0.0, 0.0}, new Double[]{0.0});
        ArrayList<Double> weights = new ArrayList<>(Arrays.asList(0.378188690056531, -0.002599863661820412, -0.3214058132248573, -0.17359481909425079, 0.28064864598843453, 0.4769421926723004,
                -0.25840772187553085, 0.23481872373112433, 0.011558058937199855, 0.4168138370103046, -0.19209689489373305, 0.18019046053621812,
                -0.22057101437576232, 0.34703097884852363, -0.10823267924609747));
        int weightIdx = 0;
        for (int level = 0; level < 3; ++level){
            for (int neuron = 0; neuron < net.neuronsAtLevel(level); ++neuron){
                for (int k = 0; k < net.getNeuron(level,neuron).getWeights().size(); ++k) {
                    net.getNeuron(level, neuron).setWeight(k, weights.get(weightIdx));
                    ++weightIdx;
                }
            }
        }
        net.setDrawWeights(false);
        net.setDrawVector(false);
        net.trainWhileErrorNotLessThan(0.1, data);
        double oneoneanswer = net.getAnswer(new ArrayList<>(Arrays.asList(1.0, 1.0))).get(0);
        double onezeroanswer = net.getAnswer(new ArrayList<>(Arrays.asList(1.0, 0.0))).get(0);
        double zerooneanswer = net.getAnswer(new ArrayList<>(Arrays.asList(0.0, 1.0))).get(0);
        double zerozeroanswer = net.getAnswer(new ArrayList<>(Arrays.asList(0.0, 0.0))).get(0);
        assertAll("Answers",
                ()->assertTrue(oneoneanswer<0.1, "1 1: "+oneoneanswer),
                ()->assertTrue(zerooneanswer>0.9, "0 1: "+zerooneanswer),
                ()->assertTrue(onezeroanswer>0.9, "1 0: "+onezeroanswer),
                ()->assertTrue(zerozeroanswer<0.1, "0 0: "+zerozeroanswer));
    }

    @Test
    void threeLevelXORTest() throws NeuralNetworkError {
        NeuralNetwork net = new NeuralNetwork(2,2,1, Neuron.ActivationFunction.UNIPOLAR, false);
        TrainingData data = new TrainingData();
        data.addTrainingVector(new Double[]{1.0, 1.0}, new Double[]{0.0});
        data.addTrainingVector(new Double[]{0.0, 1.0}, new Double[]{1.0});
        data.addTrainingVector(new Double[]{1.0, 0.0}, new Double[]{1.0});
        data.addTrainingVector(new Double[]{0.0, 0.0}, new Double[]{0.0});
        net.trainWhileErrorNotLessThan(0.1, data);
        double oneoneanswer = net.getAnswer(new ArrayList<>(Arrays.asList(1.0, 1.0))).get(0);
        double onezeroanswer = net.getAnswer(new ArrayList<>(Arrays.asList(1.0, 0.0))).get(0);
        double zerooneanswer = net.getAnswer(new ArrayList<>(Arrays.asList(0.0, 1.0))).get(0);
        double zerozeroanswer = net.getAnswer(new ArrayList<>(Arrays.asList(0.0, 0.0))).get(0);
        assertAll("Answers",
                ()->assertTrue(oneoneanswer<0.1, "1 1: "+oneoneanswer),
                ()->assertTrue(zerooneanswer>0.9, "0 1: "+zerooneanswer),
                ()->assertTrue(onezeroanswer>0.9, "1 0: "+onezeroanswer),
                ()->assertTrue(zerozeroanswer<0.1, "0 0: "+zerozeroanswer));
    }

    @Test
    void multiOutputTest() throws NeuralNetworkError {
        NeuralNetwork net = new NeuralNetwork(2,2,2, Neuron.ActivationFunction.UNIPOLAR, false);
        TrainingData data = new TrainingData();
        data.addTrainingVector(new Double[]{1.0, 1.0}, new Double[]{0.0, 1.0});
        data.addTrainingVector(new Double[]{0.0, 1.0}, new Double[]{1.0, 0.0});
        data.addTrainingVector(new Double[]{1.0, 0.0}, new Double[]{1.0, 0.0});
        data.addTrainingVector(new Double[]{0.0, 0.0}, new Double[]{0.0, 1.0});
        net.trainWhileErrorNotLessThan(0.1, data);
        double oneoneanswer = net.getAnswer(new ArrayList<>(Arrays.asList(1.0, 1.0))).get(0);
        double onezeroanswer = net.getAnswer(new ArrayList<>(Arrays.asList(1.0, 0.0))).get(0);
        double zerooneanswer = net.getAnswer(new ArrayList<>(Arrays.asList(0.0, 1.0))).get(0);
        double zerozeroanswer = net.getAnswer(new ArrayList<>(Arrays.asList(0.0, 0.0))).get(0);

        double oneonetwoanswer = net.getAnswer(new ArrayList<>(Arrays.asList(1.0, 1.0))).get(1);
        double onezerotwoanswer = net.getAnswer(new ArrayList<>(Arrays.asList(1.0, 0.0))).get(1);
        double zeroonetwoanswer = net.getAnswer(new ArrayList<>(Arrays.asList(0.0, 1.0))).get(1);
        double zerozerotwoanswer = net.getAnswer(new ArrayList<>(Arrays.asList(0.0, 0.0))).get(1);
        assertAll("Answers",
                ()->assertTrue(oneoneanswer<0.1, "1 1 < 0.1: "+oneoneanswer),
                ()->assertTrue(zerooneanswer>0.9, "0 1 > 0.9: "+zerooneanswer),
                ()->assertTrue(onezeroanswer>0.9, "1 0 > 0.9: "+onezeroanswer),
                ()->assertTrue(zerozeroanswer<0.1, "0 0 < 0.1: "+zerozeroanswer),
                ()->assertTrue(oneonetwoanswer>0.9, "1 1 > 0.9:"+oneonetwoanswer),
                ()->assertTrue(zeroonetwoanswer<0.1, "0 1 < 0.1: "+zeroonetwoanswer),
                ()->assertTrue(onezerotwoanswer<0.1, "1 0 < 0.1: "+onezerotwoanswer),
                ()->assertTrue(zerozerotwoanswer>0.9, "0 0 > 0.9: "+zerozerotwoanswer));
    }
}
