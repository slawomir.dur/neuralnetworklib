package NeuralNetworkLib;

import java.util.ArrayList;


public class NeuralNetwork {
    private int inputNumber;
    private int neuronsHighest;
    private int neuronsMiddle;
    private int neuronsOutput;
    private ArrayList<ArrayList<Neuron>> neurons = new ArrayList<>();
    private final double ro = 0.15;
    private boolean twoLevel;
    private boolean isDrawWeights = true;
    private boolean isDrawVector = true;
    private int errorCalculationInterval = 100;

    public NeuralNetwork(int inputs, int neuronsAtInput, int neuronsAtOutput, Neuron.ActivationFunction function, boolean twoLevel){
        this.inputNumber = inputs;
        this.neuronsHighest = neuronsAtInput;
        this.neuronsOutput = neuronsAtOutput;
        this.neuronsMiddle = (int)Math.ceil(Math.sqrt((double)this.neuronsHighest * this.neuronsOutput));
        this.twoLevel = twoLevel;
        prepareNeurons(function);
    }

    public int Levels(){
        return neurons.size();
    }

    public int Neurons(){
        int sum = 0;
        for (ArrayList<Neuron> list: neurons){
            sum += list.size();
        }
        return sum;
    }

    public Neuron getNeuron(int level, int index) {
        return this.neurons.get(level).get(index);
    }

    public int neuronsAtLevel(int level){
        return this.neurons.get(level).size();
    }

    private void prepareNeurons(Neuron.ActivationFunction function){
        if (!twoLevel)
        {
            ArrayList<Neuron> inputLevel = new ArrayList<>();
            ArrayList<Neuron> middleLevel = new ArrayList<>();
            ArrayList<Neuron> outputLevel = new ArrayList<>();
            //input
            for (int i = 0; i < this.neuronsHighest; ++i){
                inputLevel.add(new Neuron(this.inputNumber, function));
            }
            //middle
            for (int i = 0; i < this.neuronsMiddle; ++i){
                middleLevel.add(new Neuron(this.neuronsHighest, function));
            }
            //output
            for (int i = 0; i < this.neuronsOutput; ++i){
                outputLevel.add(new Neuron(this.neuronsMiddle, function));
            }
            //assembling
            neurons.add(inputLevel);
            neurons.add(middleLevel);
            neurons.add(outputLevel);
            //connecting input-middle
            for (int i = 0; i < this.neuronsHighest; ++i){
                Neuron neuron = this.neurons.get(0).get(i);
                for (int j = 0; j < this.neuronsMiddle; ++j){
                    Neuron connectedNeuron = this.neurons.get(1).get(j);
                    neuron.connect(new NeuralConnection(connectedNeuron, i+1));
                }
            }
            //connecting middle-output
            for (int i = 0; i < this.neuronsMiddle; ++i){
                Neuron neuron = this.neurons.get(1).get(i);
                for (int j = 0; j < this.neuronsOutput; ++j){
                    Neuron connectedNeuron = this.neurons.get(2).get(j);
                    neuron.connect(new NeuralConnection(connectedNeuron, i+1));
                }
            }
        } else {
            ArrayList<Neuron> inputLevel = new ArrayList<>();
            ArrayList<Neuron> outputLevel = new ArrayList<>();
            //input
            for (int i = 0; i < this.neuronsHighest; ++i) {
                inputLevel.add(new Neuron(this.inputNumber, function));
            }
            //output
            for (int i = 0; i < this.neuronsOutput; ++i){
                outputLevel.add(new Neuron(inputLevel.size(), function));
            }
            //assembling
            neurons.add(inputLevel);
            neurons.add(outputLevel);
            //connecting input-output
            for (int i = 0; i < this.neuronsHighest; ++i){
                Neuron neuron = this.neurons.get(0).get(i);
                for (int j = 0; j < this.neuronsOutput; ++j){
                    Neuron connectedNeuron = this.neurons.get(1).get(j);
                    neuron.connect(new NeuralConnection(connectedNeuron, i+1));
                }
            }
        }

    }

    public void setErrorCalculationInterval(int interval){
        this.errorCalculationInterval = interval;
    }

    public int getErrorCalculationInterval(){
        return this.errorCalculationInterval;
    }

    public void setDrawWeights(boolean drawWeights){
        this.isDrawWeights = drawWeights;
    }

    public void setDrawVector(boolean drawVector){
        this.isDrawVector = drawVector;
    }

    public void drawWeights(){
        for (ArrayList<Neuron> neuronsLevel : neurons) {
            for (Neuron neuron : neuronsLevel) {
                neuron.drawWeights();
            }
        }
    }

    public void train(TrainingData trainingData) throws NeuralNetworkError {
        trainTimes(trainingData, 300000);
    }

    public void trainTimes(TrainingData trainingData, int times) throws NeuralNetworkError {
        if (this.isDrawWeights) {
            drawWeights();
        }

        for (int i = 0; i < times; ++i){
            if (this.isDrawVector){
                trainingSession(trainingData, i%trainingData.size());
            } else {
                trainingSessionRandomVector(trainingData);
            }
        }
    }

    public void trainWhileErrorNotLessThan(double error, TrainingData trainingData) throws NeuralNetworkError {

        if (this.isDrawWeights) {
            drawWeights();
        }

        double realError = 100.0;
        int counter = 0;
        while (realError > error){
            if (this.isDrawVector){
                trainingSession(trainingData, counter%trainingData.size());
            } else {
                trainingSessionRandomVector(trainingData);
            }
            if (counter%this.errorCalculationInterval==0){
                realError = calculateError(trainingData);
                System.out.println(realError);
            }
            ++counter;
        }
    }

    public double calculateError(TrainingData trainingData) throws NeuralNetworkError {
        double sum = 0.0;
        for (int i = 0; i < trainingData.sizeOfExpected(); ++i) {
            for (int j = 0; j < trainingData.size(); ++j) {
                TrainingVector trainingVector = trainingData.getVector(j);
                ArrayList<Double> output = getAnswer(trainingVector.getInputs());
                sum += Math.abs(output.get(i) - trainingVector.getExpectedValues().get(i));
            }
        }
        return sum;
    }

    private void trainingSessionRandomVector(TrainingData trainingData) throws NeuralNetworkError {
        trainingSession(trainingData, -1);
    }

    private void trainingSession(TrainingData trainingData, int idx) throws NeuralNetworkError {
        TrainingVector vector = (idx == -1) ? trainingData.randomVector() : trainingData.getVector(idx);
        ArrayList<Neuron> inputLevel = this.neurons.get(0);
        for (int j = 0; j < inputLevel.size(); ++j){
            inputLevel.get(j).setInput(vector.getInputs());
        }
        forwardPropagation();
        backwardPropagation(vector.getExpectedValues());
    }

    public ArrayList<Double> getAnswer(ArrayList<Double> input) throws NeuralNetworkError {
        for (Neuron neuron: this.neurons.get(0)){
            neuron.setInput(input);
        }
        return forwardPropagation();
    }

    private ArrayList<Double> forwardPropagation() throws NeuralNetworkError {
        //calculating values of each neuron
        for (int i = 0; i < this.neurons.size(); ++i){
            ArrayList<Neuron> level = this.neurons.get(i);
            ArrayList<Double> outputs = new ArrayList<>();
            for (Neuron neuron: level){
                neuron.calculateOutput();
                outputs.add(neuron.output());
            }
            if (i != this.neurons.size()-1){
                ArrayList<Neuron> lowerLevel = this.neurons.get(i+1);
                for (Neuron neuron: lowerLevel){
                    neuron.setInput(outputs);
                }
            }
        }
        ArrayList<Double> outputs = new ArrayList<>();
        for (Neuron neuron: this.neurons.get(this.neurons.size()-1)){
            outputs.add(neuron.output());
        }
        return outputs;
    }

    private void backwardPropagation(ArrayList<Double> expectedValues) throws NeuralNetworkError {
        if (expectedValues.size() != this.neurons.get(this.neurons.size()-1).size()){
            throw new NeuralNetworkError("Number of expected values cannot differ from the number of neurons on output");
        }
        //calculating derivatives
        for (int i = 0; i < this.neurons.size(); ++i){
            ArrayList<Neuron> level = this.neurons.get(i);
            for (Neuron neuron: level){
                neuron.calculateDerivative();
            }
        }
        //calculating deltas
        for (int i = this.neurons.size()-1; i >= 0; --i){
            ArrayList<Neuron> level = this.neurons.get(i);
            if (i == this.neurons.size()-1){
                for (int n = 0; n < level.size(); ++n) {
                    level.get(n).calculateDeltaOutputLevel(expectedValues.get(n));
                }
            } else {
                for (Neuron neuron: level) {
                    neuron.calculateDeltaNonOutputLevel();
                }
            }
        }
        //updating weights
        for (ArrayList<Neuron> neurons: this.neurons){
            for (Neuron neuron: neurons){
                neuron.updateWeights(this.ro);
            }
        }
        
    }

}