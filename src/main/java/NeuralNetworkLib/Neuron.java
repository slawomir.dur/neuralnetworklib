package NeuralNetworkLib;
/**
 * @author slavic-d
 */

import java.util.ArrayList;
import java.util.Random;

public class Neuron {
    public enum ActivationFunction{
        UNIPOLAR, BIPOLAR
    }

    private final int inputNumber;
    private ArrayList<Double> inputs = new ArrayList<>();
    private ArrayList<Double> weights = new ArrayList<>();
    private ArrayList<Double> weightsDeltas = new ArrayList<>();
    private boolean isOutputReady = false;
    private double output;
    private boolean isDerivativeReady = false;
    private double derivative;
    private double delta;
    private ActivationFunction function;
    private ArrayList<NeuralConnection> outgoingConnections = new ArrayList<>();
    private final double lambda = 1.0;

    public Neuron(int inputs){
        this.inputNumber = inputs;
        prepareInputsAndWeights();
    }

    public Neuron(int inputs, ActivationFunction function){
        this.inputNumber = inputs;
        this.function = function;
        prepareInputsAndWeights();
    }

    public void setActivationFunction(ActivationFunction function){
        this.function = function;
    }

    public void setInput(ArrayList<Double> newInputs) throws NeuralNetworkError {
        if (newInputs.size() != this.inputNumber)
            throw new NeuralNetworkError("Ivalid size of argument");
        this.inputs.clear();
        this.inputs = (ArrayList<Double>) newInputs.clone();
        this.inputs.add(0, 1.0); // first element

        this.isOutputReady = false;
        this.isDerivativeReady = false;
    }

    public double getInput(int index){
        return inputs.get(index);
    }

    public ArrayList<Double> getInputs() { return inputs; }

    public double getWeight(int index){
        return weights.get(index);
    }

    public void setInput(int index, double input){
        this.inputs.set(index, input);
    }

    public void setWeight(int index, double input){
        this.weights.set(index, input);
    }

    public void drawWeights(){
        Random rand = new Random();
        for (int i = 0; i < weights.size(); ++i){
            weights.set(i, rand.nextDouble()-0.5);
        }
    }

    public double weightedSum(){
        double weightedSum = 0.0;
        for (int i = 0; i < inputs.size(); ++i){
            weightedSum += inputs.get(i) * weights.get(i);
        }
        return weightedSum;
    }

    public void calculateOutput() throws NeuralNetworkError {
        double weightedSum = weightedSum();
        if (function == ActivationFunction.UNIPOLAR){
            this.output = 1.0/(1.0+Math.pow(Math.E,-this.lambda*weightedSum));
        } else if (function == ActivationFunction.BIPOLAR) {
            this.output = (1.0-Math.pow(Math.E,-this.lambda*weightedSum))/(1+Math.pow(Math.E,-this.lambda*weightedSum));
        } else {
            throw new NeuralNetworkError("Unknown activation function type");
        }
        this.isOutputReady = true;
    }

    public double output() throws NeuralNetworkError {
        if (!this.isOutputReady)
            throw new NeuralNetworkError("Output not calculated");
        return this.output;
    }

    public void calculateDerivative() throws NeuralNetworkError {
        if (function == ActivationFunction.UNIPOLAR){
            this.derivative = this.output - Math.pow(this.output, 2);
        } else if (function == ActivationFunction.BIPOLAR) {
            this.derivative = 0.5 * (1.0 - Math.pow(this.output, 2));
        } else {
            throw new NeuralNetworkError("Unknown activation function type");
        }
        this.isDerivativeReady = true;
    }

    public double derivative() throws NeuralNetworkError {
        if (!this.isDerivativeReady)
            throw new NeuralNetworkError("Derivative not calculated");
        return this.derivative;
    }

    public void setDelta(double delta) {
        this.delta = delta;
    }

    public void setDerivative(double derivative) {
        this.derivative = derivative;
    }

    public void setDerivativeReady(boolean ready) {
        this.isDerivativeReady = ready;
    }

    public void setOutputReady(boolean ready) {
        this.isOutputReady = ready;
    }

    public void calculateDeltaOutputLevel(double expectedValue) throws NeuralNetworkError{
        if (!isOutputReady){
            throw new NeuralNetworkError("Output not calculated");
        }
        if (!isDerivativeReady){
            throw new NeuralNetworkError("Derivative not calculated");
        }
        this.delta = (expectedValue-output)*derivative;
    }

    public void calculateDeltaNonOutputLevel() throws NeuralNetworkError {
        if (!isOutputReady){
            throw new NeuralNetworkError("Output not calculated");
        }
        if (!isDerivativeReady){
            throw new NeuralNetworkError("Derivative not calculated");
        }
        double deltaSum = 0.0;
        for (int i = 0; i < outgoingConnections.size(); ++i){
            NeuralConnection connection = this.outgoingConnections.get(i);
            Neuron connectedNeuron = connection.getNeuron();
            deltaSum += connectedNeuron.getDelta() * connectedNeuron.getWeight(connection.weightIndex());
        }
        this.delta = deltaSum * derivative;
    }


    public void connect(NeuralConnection connection){
        outgoingConnections.add(connection);
    }

    public void connect(Neuron neuron, int weightIndex) { connect(new NeuralConnection(neuron, weightIndex)); }

    public void updateWeights(double ro){
        for (int i = 0; i < weights.size(); ++i) {
            double currentWeight = getWeight(i);
            setWeight(i, getWeight(i) + ro * delta * getInput(i));
            double weightDelta = getWeight(i)-currentWeight;
            weightsDeltas.set(i, weightDelta);
        }
    }

    private void prepareInputsAndWeights(){
        for (int i = 0; i < this.inputNumber+1; ++i){
            if (i == 0){
                inputs.add(1.0); // first input is always 1
            } else {
                inputs.add(0.0);
            }
            weights.add(0.0);
            weightsDeltas.add(0.0);
        }

    }

    public int getInputNumber() { return inputNumber; }

    public int getWeightsNumber() { return weights.size(); };

    public int  getInputsNumber() {return inputs.size(); }

    public int getWeightsDeltasNumber() {return weightsDeltas.size();}

    public double getWeightDelta(int index) {return weightsDeltas.get(index);}

    public ArrayList<Double> getWeights() {return weights;}

    public void setWeights(ArrayList<Double> weights) {
        this.weights = weights;
    }

    public ArrayList<Double> getWeightsDeltas() {return weightsDeltas;}

    public double getDelta(){
        return this.delta;
    }
}

class NeuralConnection {
    private Neuron neuron;
    private int weightIdx;

    NeuralConnection(Neuron neuron, int weightIndex){
        this.neuron = neuron;
        this.weightIdx = weightIndex;
    }

    public Neuron getNeuron() {
        return neuron;
    }

    public int weightIndex(){
        return weightIdx;
    }
}