package NeuralNetworkLib;

public class NeuralNetworkError extends Exception{
    public NeuralNetworkError(String msg){
        super(msg);
    }
}
