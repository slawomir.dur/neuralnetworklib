package NeuralNetworkLib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class TrainingData {
    private ArrayList<TrainingVector> trainingVectors = new ArrayList<>();

    public TrainingData(){

    }

    public TrainingData(ArrayList<TrainingVector> trainingVectors){
        this.trainingVectors = trainingVectors;
    }

    public void addTrainingVector(ArrayList<Double> inputs, ArrayList<Double> expectedValues){
        this.trainingVectors.add(new TrainingVector(inputs, expectedValues));
    }

    public void addTrainingVector(Double[] inputs, Double[] expectedValues){
        this.trainingVectors.add(new TrainingVector(inputs, expectedValues));
    }

    public TrainingVector randomVector(){
        Random random = new Random();
        int randomIndex = random.nextInt(this.trainingVectors.size());
        return this.trainingVectors.get(randomIndex);
    }

    public TrainingVector getVector(int idx){
        return trainingVectors.get(idx);
    }

    public int size(){
        return trainingVectors.size();
    }

    public int sizeOfExpected() {return this.trainingVectors.get(0).getExpectedValues().size();}
}

class TrainingVector {
    private ArrayList<Double> inputs = new ArrayList<>();
    private ArrayList<Double> expectedValues = new ArrayList<>();

    public TrainingVector(ArrayList<Double> input, ArrayList<Double> expectedValues){
        this.inputs = input;
        this.expectedValues = expectedValues;
    }

    public TrainingVector(Double[] input, Double[] expectedValues){
        this.inputs = new ArrayList<>(Arrays.asList(input));
        this.expectedValues = new ArrayList<>(Arrays.asList(expectedValues));
    }

    public ArrayList<Double> getInputs(){
        return this.inputs;
    }

    public ArrayList<Double> getExpectedValues(){
        return this.expectedValues;
    }
}
